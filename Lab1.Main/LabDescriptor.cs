﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Lab1.Contract;
using Lab1.Implementation;


namespace Lab1.Main
{
    public struct LabDescriptor
    {
        #region P1

        public static Type IBase = typeof(ICzlowiek);
        
        public static Type ISub1 = typeof(IMężczyzna);
        public static Type Impl1 = typeof(Impl1);
        
        public static Type ISub2 = typeof(IKobieta);
        public static Type Impl2 = typeof(Impl2);


        public static string baseMethod = "CommonMethod";
        public static object[] baseMethodParams = new object[] { };

        public static string sub1Method = "mowmezczyzna";
        public static object[] sub1MethodParams = new object[] { };

        public static string sub2Method = "mowKobieta";
        public static object[] sub2MethodParams = new object[] { };


        #endregion

        #region P2

        public static string collectionFactoryMethod = "FactoryMethod";
        public static string collectionConsumerMethod = "ConsumerMethod";

        #endregion

        #region P3

        public static Type IOther = typeof(IKosmita);
        public static Type Impl3 = typeof(Impl3);

        public static string otherCommonMethod = "CommonMethod";
        public static object[] otherCommonMethodParams = new object[] { };

        #endregion
    }
}
