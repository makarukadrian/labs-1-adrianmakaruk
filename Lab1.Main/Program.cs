﻿using System;
using Lab1.Contract;
using Lab1.Implementation;
using System.Collections.Generic;


namespace Lab1.Main
{
    public class Program
    {
        static void Main(string[] args)
        {
            var collection = FactoryMethod();
            ConsumerMethod(collection);

            var impl3 = new Impl3();
            Console.WriteLine(impl3.CommonMethod());
            Console.WriteLine((impl3 as IMężczyzna).CommonMethod());
            Console.WriteLine((impl3 as IKosmita).CommonMethod());

            Console.ReadLine();
        }

        public static IEnumerable<ICzlowiek> FactoryMethod()
        {
            var impl1 = new Impl1();
            var impl2 = new Impl2();
            var impl1_1 = new Impl1();

            return new List<ICzlowiek> { impl1, impl2, impl1_1 };
        }

        public static void ConsumerMethod(IEnumerable<ICzlowiek> collection)
        {
            foreach (var item in collection)
            {
                Console.WriteLine(item.CommonMethod());
            }
        }


    }
}