﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Lab1.Contract;

namespace Lab1.Implementation
{
    public class Impl3 : IMężczyzna, IKosmita
    {
        public string CommonMethod()
        {
            return "CommonMethod";
        }

        public void OtherMethod()
        {
            Console.WriteLine("OtherMethod");
        }

        string ICzlowiek.CommonMethod()
        {
            return "ICzlowiek.CommonMethod";
        }

        string IKosmita.CommonMethod()
        {
            return "IKosmita.CommonMethod";
        }

        public void mowmezczyzna()
        {
            throw new NotImplementedException();
        }
    }
}